# mirage.sh
<h4>A simple command line interface for mirage.photos written in POSIX compliant 
shell.</h4>

### Features
- Makes it easier to create scripts that upload to mirage.photos
- Interact with mirage.photos through your favourite programs with scripts
- Only depends on curl and POSIX utilities

### Non-features
- Built-in support for external programs like dmenu
- Built-in support for copying to the X clipboard
- Verbose output

### Setup
The script requires you set your mirage.photos upload key as an environment
variable. This variable being ```$MIRAGE_KEY```. It's up to you to secure your key.

You'll also need to set the target API domain, and optionally the target endpoints.
For example if you want to use pxl.blue, you'd need to set the following environemnt
variables:

```
MIRAGE_API_DOMAIN="api.pxl.blue"
MIRAGE_ENDPOINT_UPLOAD="/upload/sharex"
```

or if you want to use your own instance of mirage:

```
MIRAGE_API_DOMAIN="api.example.com"
```

You can run the script (it's portable) or stick the script somewhere in ```$PATH```. 
I personally stick it in ```~/.local/bin```.

### Usage
Some people are new to Unix and don't understand why there isn't built-in
support for things like copy+paste. This is intentional. This is intended to be
used like a Unix program. This means you can use outputs of this script with
other programs. For example, if you want to copy the link to your clipboard
you can use xclip like so:

```./mirage.sh -d mirage.re -u skeleton.gif | xclip -selection clipboard```

This will put the output of the command into your clipboard.

If you'd like to see some examples, such as a dmenu implementation and a simple
script to fetch domains and save them to a file, check out [webb/mirage-scripts](https://gitea.com/webb/mirage-scripts).

### Contributing
If you want to contribute feel free to make a pull request. You can also open an
issue.
