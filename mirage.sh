#!/bin/sh

error_conflicting() {
	printf "ERROR: Multiple operations selected. Please choose one operation to execute.\n"
	exit 1
}
error_file_does_not_exist() {
	printf "ERROR: The file specified doesn't exist!\n"
	exit 1
}
error_expected_stdin() {
	printf "ERROR: Expected data from STDIN!\n"
	exit 1
}
error_not_set() {
	printf "ERROR: Variable \$%s not set!\n" "$1"
	exit 1
}
argument_tick() {
	[ ! "$1" = "-" ] && [ ! -t 0 ] && printf "ERROR: Command-line arguments should not co-exist with data from STDIN!\n" && exit 1
	[ "$1" = "-" ] && [ -t 0 ] && printf "ERROR: Expected data from STDIN!"
	if [ -n "$argument_tick" ]; then error_conflicting; else argument_tick="set"; fi
}
print_usage() {
	printf "Usage: %s <options>\n-d: domain to use\n-f: fetch the latest domains and print result\n-r: use only the raw response of -f (use this if you want to parse the json on your own)\n-s: url to shorten\n-u: file to upload (putting a dash in this argument will read from STDIN)\n-p: file to upload as paste (can accept text from STDIN by setting this as a dash)\n-n: set filename if using STDIN with -u (by default the script will try to guess what the extension is)\n" "$0"
	exit 1
}

[ -z "$MIRAGE_ENDPOINT_UPLOAD" ] && MIRAGE_ENDPOINT_UPLOAD="/upload"
[ -z "$MIRAGE_ENDPOINT_SHORTEN" ] && MIRAGE_ENDPOINT_SHORTEN="/shorten"
[ -z "$MIRAGE_ENDPOINT_PASTE" ] && MIRAGE_ENDPOINT_PASTE="/upload/paste"
[ -z "$MIRAGE_ENDPOINT_DOMAIN" ] && MIRAGE_ENDPOINT_DOMAIN="/domains"

[ -z "$1" ] && print_usage
[ -z "$MIRAGE_API_DOMAIN" ] && error_not_set MIRAGE_API_DOMAIN
while getopts ":fd:s:u:rn:p:" opt; do
	case $opt in
	f)
		argument_tick "$OPTARG"
		will_fetch=1
		;;
	d)
		domain=$OPTARG
		;;
	\?)
		printf "ERROR: Invalid option: -$%s\n" "$OPTARG"
		exit
		;;
	:)
		printf "ERROR: Option -%s requires an argument.\n" "$OPTARG"
		exit
		;;
	s)
		argument_tick "$OPTARG"
		url_to_shorten=$OPTARG
		;;
	u)
		argument_tick "$OPTARG"
		file_arg=$OPTARG
		;;
	r)
		fetch_raw="set"
		;;
	n)
		file_name=$OPTARG
		;;
	p)
		argument_tick "$OPTARG"
		paste_arg=$OPTARG
		;;
	esac
done

if [ -n "$file_arg" ]; then
	[ -z "$MIRAGE_KEY" ] && error_not_set MIRAGE_KEY
	[ -z "$domain" ] && printf "ERROR: A domain has not been selected. Please select a domain"
	[ ! "$file_arg" = "-" ] && [ ! -f "$file_arg" ] && printf "ERROR: The file specified doesn't exist!\n" && exit 1
	[ ! -t 0 ] && [ "$file_arg" = "-" ] && temp_file=$(mktemp) && cp /dev/stdin "$temp_file" &&
		if [ -z "$file_name" ]; then
			file_extension=$(file --extension - <"$temp_file" | sed 's/.*\ //') &&
				if [ "$file_extension" = "???" ]; then
					file_extension="$(file --mime-type - <"$temp_file" | sed 's/.*\///')"
				fi
			file_name="file.$file_extension"
		fi
	[ ! -t 0 ] && curl -s -F "file=@-;filename=$file_name" -F "key=$MIRAGE_KEY" -F "host=$domain" https://"$MIRAGE_API_DOMAIN""$MIRAGE_ENDPOINT_UPLOAD" <"$temp_file" &&
		rm -rf "$temp_file" && exit_code="success"
	[ -t 0 ] && curl -s -F "file=@$file_arg" -F "key=$MIRAGE_KEY" -F "host=$domain" https://"$MIRAGE_API_DOMAIN""$MIRAGE_ENDPOINT_UPLOAD" && exit_code="success"
	printf "\n"
	[ ! "$exit_code" = "success" ] && exit 1 || exit 0

elif [ -n "$url_to_shorten" ]; then
	check_if_key_is_set
	[ "$url_to_shorten" = "-" ] && [ -t 0 ] && read -r url_to_shorten </dev/stdin &&
		[ -z "$url_to_shorten" ] && printf "ERROR: URL empty!"
	curl -s -X POST -d "host=$domain" -d "key=$MIRAGE_KEY" -d "url=$url_to_shorten" https://"$MIRAGE_API_DOMAIN""$MIRAGE_ENDPOINT_SHORTEN" && exit 0
	exit 1

elif [ -n "$will_fetch" ]; then
	curl_output="$(curl -s https://"$MIRAGE_API_DOMAIN""MIRAGE_ENDPOINT_DOMAINS")"
	[ -z "$fetch_raw" ] && printf "%s" "$curl_output" | grep -m1 -oP '\"domain\"\s*:\s*"\K[^\"]+' || printf "%s\n" "$curl_output"
	exit $?

elif [ -n "$paste_arg" ]; then
	[ ! "$paste_arg" = "-" ] && [ ! -f "$file_arg" ] && error_file_does_not_exist
	[ "$paste_arg" = "-" ] && [ ! -t 0 ] &&
		temp_file="$(mktemp)" && cp /dev/stdin "$temp_file" && curl -s -F "file=@$temp_file" -F "key=$MIRAGE_KEY" -F "host=$domain" https://"$MIRAGE_API_DOMAIN""$MIRAGE_ENDPOINT_PASTE" | grep -m1 -oP '\"url\"\s*:\s*"\K[^\"]+' && rm -rf "$temp_file" && exit 0
	[ ! "$paste_arg" = "-" ] && curl -s -F "file=@$paste_arg" -F "key=$MIRAGE_KEY" -F "host=$domain" "$MIRAGE_API_DOMAIN""$MIRAGE_ENDPOINT_PASTE" <"$file_arg" | grep -m1 -oP '\"domain\"\s*:\s*"\K[^\"]+' && exit 0
	exit 1
fi
print_usage
exit 1
